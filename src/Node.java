import java.util.*;

public class Node {

    private String name;
    private Node   firstChild;
    private Node   nextSibling;
    private int height;

    Node (String n, Node d, Node r) {
        setName (n);
        setFirstChild (d);
        setNextSibling (r);
    }

    public void   setName (String n)        { name = n; }
    public String getName()                 { return name; }
    public void   setFirstChild (Node d)    { firstChild = d; }
    public Node   getFirstChild()           { return firstChild; }
    public void   setNextSibling (Node r)   { nextSibling = r; }
    public Node   getNextSibling()          { return nextSibling; }
    public void setHeight()                 { height = 0;}
    public int getHeight()                  {return height;}


    @Override
    public String toString() {
        return leftParentheticRepresentation();
    }

    public String leftParentheticRepresentation() {
        StringBuffer b = new StringBuffer();
        b.append (getName());
        if (getFirstChild() != null) {
            b.append ("(");
            b.append (getFirstChild().leftParentheticRepresentation());
            Node right = getFirstChild().getNextSibling();
            while (right != null) {
                b.append (",");
                b.append (right.leftParentheticRepresentation());
                right = right.getNextSibling();
            }
            b.append (")");
        }
        return b.toString();
    }

    public static Node parseTree (String s) {
        if (s == null) return null;
        if (s.length() == 0) return null;
        Node root = null;
        Node curr = null;
        Node last = null;
        int state = 0; // begin
        Stack<Node> stk = new Stack<Node>();
        StringTokenizer tok = new StringTokenizer (s, "(),", true);
        while (tok.hasMoreTokens()) {
            String w = tok.nextToken().trim();
            if (w.equals ("(")) {
                state = 1; // from up
            } else if (w.equals (",")) {
                state = 2; // from left
            } else if (w.equals (")")) {
                state = 3; // from down
                stk.pop();
            } else {
                curr = new Node (w, null, null);
                switch (state) {
                    case 0: {
                        root = curr;
                        break;
                    }
                    case 1: {
                        last = stk.peek();
                        last.setFirstChild (curr);
                        break;
                    }
                    case 2: {
                        last = stk.pop();
                        last.setNextSibling (curr);
                        break;
                    }
                    default: {
                    }
                } // switch
                stk.push (curr);
            }
        } // next w
        return root;
    }

    public int maxHeight() {
        Node Root = this;
        if (this.getFirstChild() == null) {
            return 0;
        }
        else
        {
            int height = 0;
            Node child1 = null;
            Node child = this.getFirstChild();
            while (child!= null){
                height ++;
                child1 = child.getFirstChild();
                if (child1 != null){child = child1;}
                if (child1 == null){child = child.getNextSibling();}

            }
            Node sibling = Root.getNextSibling();
            Node siblingChild1 = null;
            Node siblingChild = sibling.firstChild;
            while (siblingChild!= null){
                height ++;
                siblingChild1 = siblingChild.getFirstChild();
                if (siblingChild1 != null){child = child1;}
                if (siblingChild1 == null){child = child.getNextSibling();}

            }
        }
        return height ;
    }

    public static void main (String[] param) {
        Node v = Node.parseTree ("A(B,C(D,F(K,L,M,N(O)),P))");
        System.out.println (v);
        int n = v.maxHeight();
        System.out.println ("Maximum number of levels: " + n); // 4
        // TODO!!! Your tests here!
        Node v1 = Node.parseTree ("A");
        System.out.println (v1);
        int n1 = v1.maxHeight();
        System.out.println ("Maximum number of levels: " + n1); // 0
        Node v2 = Node.parseTree ("A(B)");
        System.out.println (v2);
        int n2 = v2.maxHeight();
        System.out.println ("Maximum number of levels: " + n2); // 1
        Node v3 = Node.parseTree ("A(B(C))");
        System.out.println (v3);
        int n3 = v3.maxHeight();
        System.out.println ("Maximum number of levels: " + n3); // 2
    }
}