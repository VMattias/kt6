import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
class GraphTask {

   Graph g = new Graph("G");

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Actual main method to run examples and everything.
    */
   public void run() {
      g.createRandomSimpleGraph(7, 20);

      long startTime = System.currentTimeMillis();
      ArrayList<Vertex> results = getCenter();
      long endTime = System.currentTimeMillis();
      System.out.println(g);

      System.out.println("Vjatseslav:");
      System.out.println("Graafi keskpunktide leidmine");
      System.out.print("(All) center(s) of given graph are: ");
      for (Vertex result : results) {
         System.out.print(result.id + " ");
      }
      System.out.println("\n");


      System.out.println("Aksel:");
      System.out.println("Minimaalse pikkusega kinnine ahel");
      System.out.println(g.findClosedPath());
      System.out.println();

      System.out.println("Mattias:");
      System.out.println("Graafi ekstsentrlisuse leidmine");
      g.findEccentricity(g.first);
      System.out.println(g.first + " Eccentricity is: " + g.first.eccentricity);
      System.out.println();

      System.out.println("Maria:");
      System.out.println("Tee etteantud punktist suurima kaakuga punkti");
      System.out.println("Start Vertex: " + g.first.toString2());
      System.out.println("Destination/heaviest vertex: " + g.findBiggestVertex().toString2());
      System.out.println("Path from start to destination:");
      System.out.println(g.printPath());
      System.out.println();

      System.out.println("Alexander:");
      System.out.println("Graafi keskpunkti leidmine");
      for (Vertex s : g.centerFinding()) {
         System.out.println("Center is " + s.id);
      }
      System.out.println("That took " + (endTime - startTime) + " milliseconds");
   }

   public ArrayList<Vertex> getCenter() {

      ArrayList<Vertex> comparables = g.getVertexList(); //[v1, v2, ... , vn]

      ArrayList<Vertex> centers = new ArrayList<>();

      if (comparables.size() == 1 || comparables.size() == 2) { // if in graph exist 1 or 2 vertexes, they are centers of this graph.
         return comparables;
      }

      Integer previous_eccentricity = null; // Previous eccentricity value keeper. Created to find smallest eccentricity(Graph radius).

      for (Vertex comparable : comparables) { // Going through all the vertexes to find the smallest eccentricity.
         int present_eccentricity = g.breadthFirst(comparable);
         if (previous_eccentricity == null) {
            previous_eccentricity = present_eccentricity;
         }
         if (present_eccentricity < previous_eccentricity) {
            previous_eccentricity = present_eccentricity;
         }
      }

      for (Vertex comparable : comparables) { // Comparing all the vertexes with smallest eccentricity that was found before.
         if (previous_eccentricity == g.breadthFirst(comparable)) {
            centers.add(comparable); // Adding all vertexes with the smallest eccentricity to the list.
         }
      }
      return centers;
   }

   /**
    * Find smallest closed path in a graph where the path goes through every edge at least once.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int distance1 = 0; // keeper of data about distance.
      //edit
      private int arcCount;
      private int info;
      private Vertex previousVertex;
      public boolean visited;
      //
      private int distance = Integer.MAX_VALUE / 4;
      private LinkedList<Vertex> shortestPath = new LinkedList<>();
      private Map<Vertex, Integer> adjacentVertex = new HashMap<>();
      private Integer eccentricity;
      //
      private int weight;
      private int visitedColour = 0;

      public int getDistance() {
         return distance;
      }

      Vertex(String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;

         weight = ThreadLocalRandom.current().nextInt(1, 50 + 1);
      }

      Vertex(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public String toString2() {
         return id + ":W=" + weight;
      }

      public void setVertexInfo(int infinity) {
         this.info = infinity;
      }

      public int getVertexInfo() {
         return info;
      }

      public Vertex getVertexNext() {
         return this.next;
      }


      public void setVObject(Vertex o) {
         previousVertex = o;
      }

      public int getVertexDistance() {
         return this.distance1;
      }

      public void setVertexDistance(int i) {
         this.distance1 = i;
      }

      public List<Arc> getEdges() {
         List<Arc> list_of_edges = new ArrayList<>();
         if (this.first == null) {
            return null;
         }
         for (Arc v = this.first; v != null; v = v.next) {
            list_of_edges.add(v);
         }
         return list_of_edges;
      }

      /**
       * Get all vertex arcs.
       *
       * @return list of arcs
       */
      public ArrayList<Arc> outArcs() {
         ArrayList<Arc> arcs = new ArrayList<>();
         Arc a = first;
         while (a != null) {
            arcs.add(a);
            a = a.next;
         }

         return arcs;
      }
   }


   /**
    * Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      //edit
      private int weight;
      private int info = 0;

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      public Vertex getTarget() {
         return target;
      }

      @Override
      public String toString() {
         return id;
      }
   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      //edit
      private Vertex eulerStartPoint;
      private Vertex eulerEndPoint;
      public int radius;

      // You can add more fields, if needed

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      //edit2

      /**
       * @return List of all graph's vertexes.
       */
      public ArrayList<Vertex> findAllTheVertexesWithDFS() {
         ArrayList<Vertex> myList = new ArrayList<>();

         Vertex v = first;
         while (v != null) {
            myList.add(v);
            v = v.next;
         }
         return myList;
      }

      /**
       * finds vertex with a heaviest weight from a graph.
       * If many vertexes have the same maximum weight, first one to found is chosen
       *
       * @return vertex with biggest weight in the graph.
       */
      public Vertex findBiggestVertex() {

         ArrayList<Vertex> myListy = findAllTheVertexesWithDFS();

         try {
            myListy.get(0);    //throw RuntimeException when there are no vertexes in a graph

         } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("There are no vertexes in the given graph. " +
                    "Therefore unable to find the heaviest vertex");
         }

         int index = 0;
         int heaviest = -1;
         int heavyVertex = 0;

         for (Vertex ver : myListy) {
            if (ver.weight > heaviest) {
               heaviest = ver.weight;
               heavyVertex = index;
            }
            index++;
         }

         return myListy.get(heavyVertex);
      }

      /**
       * Prints the result of path finding method.
       * Key method of the graph.
       *
       * @return List (stack) of vertexes in the traversal order.
       */
      public Stack<Vertex> getVertexesInOrder() {

         if (!path.contains(this.first)) {  //no duplicates are allowed
            findPathToBiggest(this.first, null);
         }

         return path;
      }


      private Stack<Vertex> path = new Stack<>();  // will store the path while examining graph.
      private boolean heaviestVertexFound = false;


      /**
       * Void method that finds correct path from given start vertex to destination vertex.
       */
      public void findPathToBiggest(Vertex inputV, Vertex parentV) {

         Vertex heaviestV = findBiggestVertex();

         if (inputV.equals(heaviestV)) {   //Found heaviest vertex
            path.push(inputV);
            heaviestVertexFound = true;
            return;
         }

         if (inputV.visitedColour == 1) {  // gray vertexes
            return;
         }

         if (inputV.visitedColour == 0) {  //white vertexes
            path.push(inputV);
            inputV.visitedColour = 1;

            Arc a = inputV.first;
            while (a != null && !heaviestVertexFound) {   //processes neighbour vertexes of inputV
               if (a.target != parentV) {
                  findPathToBiggest(a.target, inputV); //parent --> inputV
               }
               a = a.next;

            }
         }

         if (!heaviestVertexFound) {
            path.pop();
         }

      }

      /**
       * Creates path from start to destination with String representation.
       * Using getVertexesInOrder() method to provide correct arcs path.
       */
      public String printPath() {

         if (getVertexesInOrder().size() < 2) {
            try {
               findAllTheVertexesWithDFS().get(0);     //when graph or path has no vertexes
            } catch (RuntimeException e) {
               throw new RuntimeException("There are no vertexes in the given graph. " +
                       "Therefore unable to find the heaviest vertex");
            }
            if (getVertexesInOrder().size() == 1) {
               return "Path or Graph has only one vertex, and graph has no cycles, therefore " +
                       "start is also destination. \n The lonely vertex: " + first.toString();
            }

         }
         StringBuilder sb = new StringBuilder();
         Stack<Vertex> correctOrder = getVertexesInOrder();

         for (int i = 0; i < correctOrder.size() - 1; i++) {
            sb.append(correctOrder.get(i).toString2());
            sb.append(" --> a");
            sb.append(createArc(correctOrder.get(i).toString(), correctOrder.get(i), correctOrder.get(i + 1), 1));
            sb.append("_");
            sb.append(createArc(correctOrder.get(i + 1).toString(), correctOrder.get(i), correctOrder.get(i + 1), 1));
            sb.append("\n");

         }
         if (correctOrder.size() >= 2) {
            sb.append(findBiggestVertex().toString2());
            sb.append("-->");
         }

         return sb.toString();
      }


      /**
       * Find the smallest(in length/weight) closed path that goes through every edge at least once.
       *
       * @return fullPath list
       */

      public int breadthFirst(Vertex s) {
         if (getVertexList() == null)
            throw new RuntimeException("No vertices defined!");
         if (!getVertexList().contains(s))
            throw new RuntimeException("Wrong argument to traverse!");
         if (getVertexList().isEmpty())
            throw new RuntimeException("Graph is empty!");
         if (getVertexList().size() == 1)
            return 0;
         int eccentricity = 0; // eccentricity value keeper.
         for (Vertex v : getVertexList()) {
            v.setVertexInfo(0); // reset / set all vertexes to be "white".
            v.setVertexDistance(0); // reset all distances.
         }
         List vq = Collections.synchronizedList(new LinkedList());
         vq.add(s);
         s.setVertexInfo(1); // set Vertex info to "grey".
         while (vq.size() > 0) {
            Vertex v = (Vertex) vq.remove(0); // breadth == FIFO.
            v.setVertexInfo(2); // set Vertex info to "black".
            Iterator eit = v.getEdges().iterator();
            while (eit.hasNext()) {
               Arc a = (Arc) eit.next();
               Vertex w = a.getTarget();
               if (w.getVertexInfo() == 0) { // if it's "white", it's possible to work with this vertex.
                  w.setVertexDistance(v.getVertexDistance() + 1); //
                  if (eccentricity < w.getVertexDistance())
                     eccentricity = w.getVertexDistance();
                  vq.add(w);
                  w.setVertexInfo(1);
               }
            }
         }
         return eccentricity;
      }

      public ArrayList<Vertex> getVertexList() {
         Vertex vertex = this.first;
         ArrayList<Vertex> list_of_vertexes = new ArrayList<>();
         if (vertex == null) // Check if there is a problem with creating a graph.
            throw new RuntimeException("Vertex is null!");
         list_of_vertexes.add(vertex);
         while (vertex.next != null) { // Adds all the vertexes of the graph to the list.
            Vertex next_vertex = vertex.getVertexNext();
            list_of_vertexes.add(next_vertex);
            vertex = next_vertex;
         }
         return list_of_vertexes;
      }

      public ArrayList<Arc> findClosedPath() {
         Graph g = this;
         ArrayList<Arc> fullPath;

         if (g.hasEulerCircuit()) {

            System.out.println("Euler circuit");
            fullPath = getEulerCycle(this.first);
         } else if (g.hasEulerPath()) {

            g.shortestPathsFrom(g.first);
            Vertex startPoint = g.eulerStartPoint;
            Vertex endPoint = g.eulerEndPoint;
            g.shortestPathsFrom(startPoint);

            ArrayList<Arc> shortestPath = new ArrayList<>();
            Arc arc = endPoint.first;
            Vertex vertex = endPoint;

            while (!vertex.id.equals(startPoint.id)) {
               while (!arc.target.id.equals(vertex.previousVertex.id)) {
                  arc = arc.next;
               }
               shortestPath.add(arc);
               vertex = vertex.previousVertex;
               arc = arc.target.first;
            }


            System.out.println("Shortest path from " + endPoint + " to " + startPoint + " : " + shortestPath);
            fullPath = g.getEulerPath();
            fullPath.addAll(shortestPath);
         } else {
            fullPath = new ArrayList<>();
            System.out.println("No path found.");
         }
         return fullPath;
      }

      /**
       * Get all vertices.
       *
       * @return vertices
       */
      public ArrayList<Vertex> getVertices() {
         ArrayList<Vertex> vertices = new ArrayList<>();
         Vertex v = first;
         while (v != null) {
            vertices.add(v);
            v = v.next;
         }
         return vertices;
      }

      Graph(String s) {
         this(s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuffer sb = new StringBuffer(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.id);
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc(String aid, Vertex from, Vertex to, int weight) {
         Arc res = new Arc(aid);
         //edit
         res.weight = weight;
         from.arcCount += 1;
         to.arcCount += 1;

         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + String.valueOf(n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               //edit
               int weight = (int) (Math.random() * n);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i], weight);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr], weight);
            } else {
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }


      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       *
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            //edit (weight added to arcs)
            int weight = 1 + (int) (Math.random() * n * 2);
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj, weight);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi, weight);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Get all vertices' degrees through graph's matrix.
       *
       * @return vertices' degree map
       */
      public Map<String, Integer> getVertexDegreesThroughMatrix() {
         Map<String, Integer> vertexDegreeMap = new HashMap<>();
         int[][] graphMatrix = createAdjMatrix();
         for (int i = 0; i < graphMatrix.length; i++) {
            for (int j = 0; j < graphMatrix[i].length; j++) {
               if (graphMatrix[i][j] != 0) {
                  String key = "v" + (i + 1);
                  if (vertexDegreeMap.containsKey(key)) {
                     vertexDegreeMap.put("v" + (i + 1), vertexDegreeMap.get(key) + graphMatrix[i][j]);
                  } else {
                     vertexDegreeMap.put("v" + (i + 1), graphMatrix[i][j]);
                  }
               }
            }
         }

         return vertexDegreeMap;
      }


      /**
       * Get all vertices' degrees.
       *
       * @return vertices' degree map
       */
      public Map<String, Integer> getVerticesDegrees() {
         Map<String, Integer> vertexDegreeMap = new HashMap<>();
         Vertex vertex = first;
         while (vertex != null) {
            vertexDegreeMap.put(vertex.id, vertex.arcCount / 2);
            vertex = vertex.next;
         }

         return vertexDegreeMap;
      }

      /**
       * https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * Shortest paths from a given vertex. Uses Dijkstra's algorithm.
       * For each vertex vInfo is length of shortest path from given
       * source s and vObject is previous vertex from s to this vertex.
       *
       * @param s source vertex
       */
      public void shortestPathsFrom(Vertex s) {
         if (getVertices() == null) return;
         int INFINITY = Integer.MAX_VALUE / 4; // big enough!!!

         Iterator vit = getVertices().iterator();
         while (vit.hasNext()) {
            Vertex v = (Vertex) vit.next();
            v.setVertexInfo(INFINITY);
            v.setVObject(null);
         }
         s.setVertexInfo(0);
         List vq = Collections.synchronizedList(new LinkedList());
         vq.add(s);
         while (vq.size() > 0) {
            int minLen = INFINITY;
            Vertex minVert = null;
            Iterator it = vq.iterator();
            while (it.hasNext()) {
               Vertex v = (Vertex) it.next();
               if (v.getVertexInfo() < minLen) {
                  minVert = v;
                  minLen = v.getVertexInfo();
               }
            }
            if (minVert == null)
               throw new RuntimeException("error in Dijkstra!!");
            if (vq.remove(minVert)) {
            } else
               throw new RuntimeException("error in Dijkstra!!");
            it = minVert.outArcs().iterator();
            while (it.hasNext()) {
               Arc e = (Arc) it.next();
               int newLen = minLen + e.weight;
               Vertex to = e.target;
               if (to.getVertexInfo() == INFINITY) {
                  vq.add(to);
               }
               if (newLen < to.getVertexInfo()) {
                  to.setVertexInfo(newLen);
                  to.setVObject(minVert);
               }
            }
         }
      } // end of shortestPathsFrom()

      /**
       * Get total weight/length of the graph if the graph has arcs with weight/length.
       *
       * @return total weight/length of arcs.
       */
      public int getTotalWeight() {
         int totalWeight = 0;
         Vertex v = first;
         Arc a;

         while (v != null) {
            a = v.first;
            while (a != null) {
               totalWeight += a.weight;
               System.out.println(a.id + " weight: " + a.weight);
               a = a.next;
            }
            v = v.next;
         }

         return totalWeight / 2;
      }

      /**
       * Check if Graph has an Euler Circuit
       *
       * @return boolean
       */
      public boolean hasEulerCircuit() {
         Vertex v = first;
         while (v != null) {
            if ((v.arcCount / 2) % 2 != 0) {
               return false;
            }
            v = v.next;
         }
         return true;
      }

      /**
       * Check if Graph has an Euler Path
       *
       * @return boolean
       */
      public boolean hasEulerPath() {
         int numberOfOdds = 0;
         Vertex v = first;
         while (v != null) {
            if ((v.arcCount / 2) % 2 != 0) {
               numberOfOdds++;
               if (numberOfOdds > 2) {
                  return false;
               } else if (numberOfOdds == 1) {
                  eulerStartPoint = v;
               } else {
                  eulerEndPoint = v;
               }
            }
            v = v.next;
         }
         return true;
      }

      /**
       * Find an euler path (if it exists).
       *
       * @return euler path list
       */
      public ArrayList<Arc> getEulerPath() {
         Vertex v = this.first;
         while (v != null) {
            if ((v.arcCount / 2) % 2 != 0) {
               return getEulerPathWithStartEnd(v);
            }
            v = v.next;
         }

         return null;
      }

      /**
       * Check if the target vertex is valid by checking its available arc paths.
       *
       * @param target       target vertex.
       * @param traveledArcs list of already traveled arcs.
       * @param endPoint     euler cycle ending vertex.
       * @return boolean
       */
      public boolean isValidTarget(Vertex target, ArrayList<String> traveledArcs, Vertex endPoint) {
         ArrayList<String> targetArcs = new ArrayList<>();
         ArrayList<String> arcs = new ArrayList<>();
         Arc a = target.first;
         while (a != null) {
            targetArcs.add(a.id);
            a = a.next;
         }

         for (int i = 0; i < targetArcs.size(); i++) {
            if (!traveledArcs.contains(targetArcs.get(i))) {
               arcs.add(targetArcs.get(i));
            }
         }

         if (arcs.size() == 2) {
            if (arcs.get(0).contains(endPoint.id) || arcs.get(1).contains(endPoint.id)) {
               if (arcs.get(0).contains("a" + endPoint.id) || arcs.get(1).contains("a" + endPoint.id)) {
                  return true;
               }
               return false;
            }
         }

         return true;
      }

      /**
       * Get an euler cycle with a starting Vertex.
       *
       * @param startVertex starting vertex for Euler Path
       * @return euler path list
       */
      public ArrayList<Arc> getEulerCycle(Vertex startVertex) {
         ArrayList<Arc> arcPath = new ArrayList<>();
         ArrayList<String> traveledArcs = new ArrayList<>();

         Map<String, Integer> vertMap = getVerticesDegrees();
         Vertex v = startVertex;

         vertMap.put(v.id, vertMap.get(v.id) - 1);
         int mapSize = vertMap.size();

         Arc arc = v.first;
         while (mapSize != 1) {

            while (vertMap.get(arc.target.id) > 1 && !traveledArcs.contains(arc.id)) {
               if (mapSize > 2 && vertMap.get(startVertex.id) == 1) {
                  if (!arc.id.contains("a" + v.id)) {
                     while (!isValidTarget(arc.target, traveledArcs, v)) {
                        arc = arc.next;
                     }
                  }
               }

               if (arc.target.id.equals(startVertex.id)) {
                  Arc a = arc.next;

                  while (a != null) {
                     if (!traveledArcs.contains(a.id)) {
                        arc = a;
                        break;
                     } else {
                        a = a.next;
                     }
                  }
               }

               arcPath.add(arc);
               String[] oppositeArc = arc.id.replace("a", "").split("_");
               String opArc = "a" + oppositeArc[1] + "_" + oppositeArc[0];
               traveledArcs.add(arc.id);
               traveledArcs.add(opArc);
               vertMap.put(arc.target.id, vertMap.get(arc.target.id) - 2);
               if (vertMap.get(arc.target.id) == 0) {
                  mapSize--;
               }
               arc = arc.target.first;
            }
            if (mapSize != 1) {
               arc = arc.next;
            }
         }

         while (vertMap.get(arc.target.id) != 1) {
            arc = arc.next;
         }
         arcPath.add(arc);


         return arcPath;
      }

      /**
       * Get an euler path with an odd starting point(Vertex has odd number of degrees).
       *
       * @param startVertex starting vertex for Euler Path
       * @return euler path list
       */
      public ArrayList<Arc> getEulerPathWithStartEnd(Vertex startVertex) {
         ArrayList<Arc> arcPath = new ArrayList<>();
         ArrayList<String> traveledArcs = new ArrayList<>();
         Map<String, Integer> vertMap = getVerticesDegrees();
         Vertex v = startVertex;
         int mapSize = vertMap.size();
         vertMap.put(startVertex.id, vertMap.get(startVertex.id) - 1);
         Arc arc = v.first;
         while (mapSize != 1) {
            while (vertMap.get(arc.target.id) > 1 && !traveledArcs.contains(arc.id)) {
               if (arc.target.id.equals(eulerEndPoint.id)) {
                  Arc a = arc.next;

                  while (a != null) {
                     if (!traveledArcs.contains(a.id)) {
                        arc = a;
                        break;
                     } else {
                        a = a.next;
                     }
                  }
               }
               arcPath.add(arc);
               String[] oppositeArc = arc.id.replace("a", "").split("_");
               String opArc = "a" + oppositeArc[1] + "_" + oppositeArc[0];
               traveledArcs.add(arc.id);
               traveledArcs.add(opArc);
               vertMap.put(arc.target.id, vertMap.get(arc.target.id) - 2);
               if (vertMap.get(arc.target.id) == 0) {
                  mapSize--;
               }
               arc = arc.target.first;
            }
            if (mapSize != 1) {
               arc = arc.next;
            }
         }

         while (vertMap.get(arc.target.id) != 1) {
            arc = arc.next;
         }

         return arcPath;
      }

      public ArrayList<Vertex> centerFinding() {
         ArrayList<Vertex> c = new ArrayList<>();
         for (Vertex vert : getVertices()) {
            if (radiusFinding() == breadthFirst(vert)) {
               c.add(vert);
            }
         }
         return c;
      }

      public int radiusFinding() {
         radius = Integer.MAX_VALUE;
         for (Vertex vertex : getVertices()) {
            if (breadthFirst(vertex) < radius) {
               radius = breadthFirst(vertex);
            }
         }
         return radius;
      }

      private void CalculateMinimumDistance(Vertex evaluationVertex, Integer edgeWeigh, Vertex sourceVertex) {
         Integer sourceDistance = sourceVertex.getDistance();
         if (sourceDistance + edgeWeigh < evaluationVertex.getDistance()) {
            evaluationVertex.distance = sourceDistance + edgeWeigh;
            LinkedList<Vertex> shortestPath = new LinkedList<>(sourceVertex.shortestPath);
            shortestPath.add(sourceVertex);
            evaluationVertex.shortestPath = shortestPath;
         }
      }

      /**
       * Gets all neighbouring vertexes to currentVertex.
       *
       * @param currentVertex
       * @return
       */

      public Map<Vertex, Integer> getAdjacentVertex(Vertex currentVertex) {
         Arc a = currentVertex.first;
         currentVertex.adjacentVertex.put(a.target, 1);
         while (a != null) {
            currentVertex.adjacentVertex.put(a.target, 1);
            a = a.next;
         }
         return currentVertex.adjacentVertex;
      }

      /**
       * Gets vertex that has the smallest distance to source vertex.
       *
       * @param unsettledVertex
       * @return
       */

      private Vertex getLowestDistanceVertex(Set<Vertex> unsettledVertex) {
         Vertex lowestDistanceVertex = null;
         int lowestDistance = Integer.MAX_VALUE / 4;
         for (Vertex Vertex : unsettledVertex) {
            int VertexDistance = Vertex.getDistance();
            if (VertexDistance < lowestDistance) {
               lowestDistance = VertexDistance;
               lowestDistanceVertex = Vertex;
            }
         }
         return lowestDistanceVertex;
      }

      /**
       * Finds the eccentricity of the vertex.
       *
       * @param start
       */
      public void findEccentricity(Vertex start) {
         // You can add more fields, if needed Mattias added 1
         Vertex[] vertexArray = getVertexList().toArray(new Vertex[0]);
         if (!getVertexList().contains(start)) {
            throw new RuntimeException("No vertex given, cannot run algorithm");
         }
         ;
         ArrayList<Integer> shortestPaths = new ArrayList<>();
         Set<Vertex> settledVertex = new HashSet<>();
         Set<Vertex> unsettledVertex = new HashSet<>();
         unsettledVertex.add(start);
         start.distance = 0;

         while (unsettledVertex.size() != 0) {
            Vertex currentVertex = getLowestDistanceVertex(unsettledVertex);
            unsettledVertex.remove(currentVertex);

            for (Map.Entry<Vertex, Integer> adjacencyPair : getAdjacentVertex(currentVertex).entrySet()) {
               Vertex adjacentVertex = adjacencyPair.getKey();
               Integer distance = adjacencyPair.getValue();
               if (!settledVertex.contains(adjacentVertex)) {
                  CalculateMinimumDistance(adjacentVertex, distance, currentVertex);
                  unsettledVertex.add(adjacentVertex);
               }
            }
            settledVertex.add(currentVertex);
         }
         for (Vertex v : vertexArray) {
            if (v != start) {
               shortestPaths.add(v.shortestPath.size());
            }
         }
         start.eccentricity = Collections.max(shortestPaths);


      }
   }
}

